local opt = vim.opt
local keyset = vim.keymap.set

-- Options
opt.showmatch = true
opt.ignorecase = true
opt.hlsearch = true
opt.incsearch = true
opt.autoindent = true
opt.number = true
opt.ttyfast = true
opt.copyindent = true
opt.smarttab = true
opt.tabstop = 4
opt.softtabstop = 4
opt.shiftwidth = 4
opt.history = 9999
opt.undolevels = 9999
opt.go = { "a" }
opt.cmdheight = 0
opt.wildmode = { "longest", "list" }
opt.clipboard = { "unnamed" }
opt.fileencodings = { "utf-8", "windows-1251", "iso-8859-15", "koi8-r" }
opt.sessionoptions = { "curdir", "buffers", "tabpages", "folds", "options", "resize", "globals", "localoptions" }

-- Edit keybindings
keyset("n", "x", '"_x', { noremap = true })
keyset("n", "D", '"_D', { noremap = true })
keyset("n", "d", '"_d', { noremap = true })
keyset("v", "d", '"_d', { noremap = true })
keyset("n", "<leader>d", '"*d', { noremap = true })
keyset("n", "<leader>D", '"*D', { noremap = true })
keyset("v", "<leader>d", '"*d', { noremap = true })

-- Packages
local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
if not vim.loop.fs_stat(lazypath) then
	vim.fn.system({
		"git",
		"clone",
		"--filter=blob:none",
		"https://github.com/folke/lazy.nvim.git",
		"--branch=stable",
		lazypath,
	})
end
vim.opt.rtp:prepend(lazypath)
require("lazy").setup("plugins")
