local km = vim.keymap

vim.g.rooter_silent_chdir = 1
vim.g.rooter_patterns = { ".git", ".svn", "package.json", "!node_modules" }
function get_project_root_path()
	vim.api.nvim_call_function("FindRootDirectory", {})
end

km.set("n", "<leader>s", function()
	require("telescope.builtin").live_grep({ cwd = get_project_root_path() })
end, { desc = "Find string" })
km.set("n", "<leader>f", function()
	require("telescope.builtin").find_files({ cwd = get_project_root_path() })
end, { desc = "Find files" })
km.set("n", "<leader>tr", function()
	require("telescope.builtin").treesitter({ cwd = get_project_root_path() })
end, { desc = "Treesitter list" })
km.set("n", "<leader>cm", function()
	require("telescope.builtin").commands({ cwd = get_project_root_path() })
end, { desc = "Command list" })
km.set("n", "<leader>tg", function()
	require("telescope.builtin").diagnostics({ cwd = get_project_root_path() })
end, { desc = "Telescope diagnositc" })

local actions = require("telescope.actions")
require("telescope").setup({
	defaults = {
		file_ignore_patterns = { "node%_modules" },
		sorting_strategy = "descending",
		prompt_prefix = "   ",
		winblend = 25,
		mappings = {
			i = {
				["<esc>"] = actions.close,
				["<C-q>"] = actions.smart_send_to_qflist + actions.open_qflist,
			},
		},
	},
	pickers = {
		find_files = {
			theme = "ivy",
		},
		live_grep = {
			theme = "ivy",
		},
		buffers = {
			theme = "ivy",
			sort_mru = true,
			ignore_current_buffer = true,
			mappings = {
				i = {
					["<C-w>"] = "delete_buffer",
				},
				n = {
					["<C-w>"] = "delete_buffer",
				},
			},
		},
	},
})
