return {
	disable_netrw = true,
	hijack_netrw = true,
	view = {
		number = true,
		relativenumber = true,
	},
	update_focused_file = {
		enable = true,
		update_cwd = true,
	},
	filters = {
		custom = { ".git", "node_modules", "dist", "lib" },
	},
}
