require("formatter").setup {
	filetype = {
		lua = {
			require("formatter.filetypes.lua").stylua,
		},
		typescript = {
			require("formatter.filetypes.typescript").prettier,
		},
		javascript = {
			require("formatter.filetypes.javascript").prettier,
		},
		json = {
			require("formatter.filetypes.json").prettier,
		},
		vue = {
			require("formatter.filetypes.vue").prettier,
		},
		markdown = {
			require("formatter.filetypes.markdown").prettier,
		},
	},
}

vim.api.nvim_create_autocmd("BufWritePre", {
	pattern = { "*.lua", "*.ts", "*.js", "*.json", "*.vue", "*.md" },
	command = "FormatWrite",
})
