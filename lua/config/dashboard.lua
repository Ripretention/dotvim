return {
	theme = "hyper",
	config = {
		header = {
			"    ____  _                 __             __  _           ",
			"   / __ \\(_)___  ________  / /____  ____  / /_(_)___  ____ ",
			"  / /_/ / / __ \\/ ___/ _ \\/ __/ _ \\/ __ \\/ __/ / __ \\/ __ \\",
			" / _, _/ / /_/ / /  /  __/ /_/  __/ / / / /_/ / /_/ / / / /",
			"/_/ |_/_/ .___/_/   \\___/\\__/\\___/_/ /_/\\__/_/\\____/_/ /_/ ",
			"       /_/                                                 ",
		},
		shortcut = {
			{ desc = "Open config", group = "@property", action = "edit $MYVIMRC", key = "c" },
			{ desc = " Update", group = "@property", action = "Lazy update", key = "u" },
		},
	},
}
