local M = {}
function M.load()
  local null_ls = require("null-ls")
  local lSsources = {
    null_ls.builtins.formatting.prettier,
    null_ls.builtins.formatting.stylua.with({
      filetypes = {
        "lua",
      },
      args = { "--indent-width", "2", "--indent-type", "Spaces", "-" },
    }),
  }

  local augroup = vim.api.nvim_create_augroup("LspFormatting", {})
  require("null-ls").setup({
    sources = lSsources,
    root_dir = require("null-ls.utils").root_pattern(
      "tsconfig.json",
      "package.json",
      ".null-ls-root",
      "Makefile",
      ".git"
    ),
    on_attach = function(client, bufnr)
      if client.supports_method("textDocument/formatting") then
        vim.api.nvim_clear_autocmds({ group = augroup, buffer = bufnr })
        vim.api.nvim_create_autocmd("BufWritePre", {
          group = augroup,
          buffer = bufnr,
          callback = function()
            vim.lsp.buf.format({
              bufnr = bufnr,
              filter = function(client)
                return client.name == "null-ls"
              end,
            })
          end,
        })
      end
    end,
  })
end

return M
