local content = {
  "Commands help:",
  "K - show documentation",
  "gd - go to definition of word under cursor",
  "gy - go to type definition of word under cursor",
  "gi - go to implementation",
  "gi - find references",
  "]g - next error",
  "[g - prev error",
  "\\cd - get diagnostics",
  "\\cf - format selected code",
  "\\cr - rename",
  "\\cc - commands",
  "\\xx - open trouble (q - for quite)",
}

local keyset = vim.keymap.set

keyset("n", "[d", vim.diagnostic.goto_prev)
keyset("n", "]d", vim.diagnostic.goto_next)
keyset("n", "<leader>cd", vim.diagnostic.setloclist)
keyset("n", "gd", vim.lsp.buf.definition)
keyset("n", "K", vim.lsp.buf.hover)
keyset("n", "gi", vim.lsp.buf.implementation)
keyset("n", "<leader>cr", vim.lsp.buf.rename)
keyset("n", "<leader>cc", vim.lsp.buf.code_action)

vim.api.nvim_create_user_command("LspHelp", function()
  local buf = vim.api.nvim_create_buf(false, true)
  vim.api.nvim_buf_set_lines(buf, 0, -1, false, content)
  vim.api.nvim_open_win(buf, true, {
    height = 600,
    width = 400,
    col = 0,
    row = 0,
    relative = "cursor",
    style = "minimal",
  })
end, { bang = true })
