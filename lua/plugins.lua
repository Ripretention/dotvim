return {
	"equalsraf/neovim-gui-shim",
	"airblade/vim-rooter",
	"wakatime/vim-wakatime",
	{
		"folke/trouble.nvim",
		config = function()
			require("config.trouble")
		end,
	},
	{
		"hrsh7th/nvim-cmp",
		dependencies = {
			{ "hrsh7th/cmp-nvim-lsp" },
			{ "hrsh7th/cmp-nvim-lua" },
			{ "hrsh7th/cmp-buffer" },
			{ "hrsh7th/cmp-path" },
			{ "hrsh7th/cmp-cmdline" },
			{ "hrsh7th/vim-vsnip" },
			{ "hrsh7th/cmp-vsnip" },
			{ "hrsh7th/vim-vsnip-integ" },
			{ "hrsh7th/cmp-calc" },
			{ "hrsh7th/cmp-nvim-lsp-signature-help" },
			{ "rafamadriz/friendly-snippets" },
		},
		config = function()
			require("config.cmp")
		end,
		event = "InsertEnter",
	},
	{
		"nvim-telescope/telescope.nvim",
		dependencies = { "nvim-lua/plenary.nvim" },
		config = function()
			require("config.telescope")
		end,
	},
	{
		"williamboman/mason.nvim",
		dependencies = {
			{
				"williamboman/mason-lspconfig.nvim",
				config = function()
					require("mason").setup()
					require("mason-lspconfig").setup({
						ensure_installed = { "tsserver", "lua_ls", "volar", "jsonls" },
						handlers = {
							function(server_name)
								require("lspconfig")[server_name].setup({})
							end,
						},
					})
				end,
				dependencies = {
					{
						"neovim/nvim-lspconfig",
						config = function()
							require("config.lspconfig")
						end,
					},
				},
			},
		},
	},
	{
		"stevearc/conform.nvim",
		opts = {
			formatters_by_ft = {
				lua = { "stylua" },
				javascript = { "prettier" },
				typescript = { "prettier" },
				json = { "prettier" },
				vue = { "prettier" },
				markdown = { "prettier" },
			},
			log_level = vim.log.levels.ERROR,
			format_after_save = {
				lsp_fallback = true,
			},
		},
		config = function(_, opts)
			require("conform").setup(opts)
		end,
	},
	{
		"nvim-treesitter/nvim-treesitter",
		opts = require("config.syntax"),
		config = function()
			vim.cmd("TSEnable highlight")
		end,
	},
	{
		"kyazdani42/nvim-tree.lua",
		init = function()
			vim.keymap.set("", "<leader>n", ":NvimTreeToggle<cr>", { silent = true })
			vim.keymap.set("", "<leader>т", ":NvimTreeToggle<cr>", { silent = true })
		end,
		opts = require("config.nvimtree"),
	},
	{ "romgrk/barbar.nvim", config = require("config.barbar") },
	"kyazdani42/nvim-web-devicons",
	{
		"glepnir/dashboard-nvim",
		opts = require("config.dashboard"),
	},
	{
		"nvim-lualine/lualine.nvim",
		config = function()
			require("config.lualine")
		end,
	},
	"lewis6991/gitsigns.nvim",
	{
		"windwp/nvim-autopairs",
		opts = {},
	},
	"machakann/vim-sandwich",
	{
		"catppuccin/nvim",
		lazy = false,
		priority = 1000,
		config = function()
			vim.g.material_style = "palenight"
			vim.cmd("colorscheme catppuccin")
			return {
				flavour = "mocha",
				integrations = { barbar = true, cmp = true, gitsigns = true, nvimtree = true, treesitter = true },
			}
		end,
	},
}
