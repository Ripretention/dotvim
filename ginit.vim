set guifont=Hack\ NFM:h12

" Neovide settings
if exists("g:neovide")
  let g:neovide_scroll_animation_length = 0.025
  let g:neovide_cursor_animation_length = 0.050
  let g:neovide_cursor_animate_in_insert_mode = v:false
endif
